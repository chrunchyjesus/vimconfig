" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^linux\|^Eterm'
  set t_Co=16
endif

try
    colorscheme onehalfdark
catch /^Vim\%((\a\+)\)\=:E185/
    " colorscheme does not exist
endtry

if exists('&winhighlight')
    setlocal winhighlight=NormalFloat:WhichKeyFloating
endif
