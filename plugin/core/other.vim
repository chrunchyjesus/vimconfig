" return to the same line when reopening a file
augroup line_return
    autocmd!
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \    execute 'normal! g`"zvzz' |
    \ endif
augroup END

" numbers starting with a zero will not be recognized as a octal
set nrformats-=octal

" some timeout thingy
if !has('nvim') && &ttimeoutlen == -1
    set ttimeout
    set ttimeoutlen=100
endif

if &shell =~# 'fish$' && (v:version < 704 || v:version == 704 && !has('patch276'))
  set shell=/usr/bin/env\ bash
endif

set autoread

if &history < 1000
  set history=1000
endif
set undolevels=1000

if &tabpagemax < 50
  set tabpagemax=50
endif
if !empty(&viminfo)
  set viminfo^=!
endif
set sessionoptions-=options

set splitbelow " Horizontal split below current.
set splitright " Vertical split to right of current.

" always use system clipboard
" TODO: make this work
set clipboard+=unnamedplus

" enable mouse support
set mouse=a

" saves undo history to a file
if has('persistent_undo')
    set undofile
endif
