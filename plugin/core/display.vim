set number relativenumber
set showmatch
set showbreak=+++
set cursorline
"set listchars=tab:>-,trail:·
set list " show problematic characters
set ruler
set laststatus=2

" Blink cursor on error instead of beeping
set visualbell

" fill column indicator
set colorcolumn=100

set display+=lastline

if &encoding ==# 'latin1' && has('gui_running')
  scriptencoding=utf-8
endif

" Tell Vim which characters to show for expanded TABs,
" trailing whitespace, and end-of-lines.
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

" Also highlight all tabs and trailing whitespace characters.
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+$\|\t/

if !&scrolloff
  set scrolloff=3 " Show next 3 lines while scrolling.
endif
if !&sidescrolloff
  set sidescrolloff=5 " Show next 5 columns while side-scrolling.
endif

set nostartofline " Do not jump to first character with page commands.
set inccommand=split " make :s interactive
